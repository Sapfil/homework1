package ru.innopolis;

/**
 * Checking input char and return type of symbol.
 * "Symbol-type" is enum of this class.
 * @author  Andrey Kostrov on 11.11.2016.
 */
public class CharValidator {

    /** constructor is set private to avoid exemplar creation */
    private CharValidator(){}

    /** symbol types */
    enum symbolType{SPACE, MINUS, NUMBER, NOT_VALID}

    /**  Checking input char and return type of symbol. */
    public static symbolType validateIntChar(char currentSymbol){

        if (currentSymbol == ' ')      // *** space-char found
            return  symbolType.SPACE;

        else if (currentSymbol > 47 && currentSymbol < 58 ) // *** number-char found
            return symbolType.NUMBER;

        else if (currentSymbol == '-')  // *** minus-char found
            return  symbolType.MINUS;

        else return symbolType.NOT_VALID; // *** not-valid-symbol-char found
    }
}
