package ru.innopolis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.boxes.BooleanBox;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Program finding positive n%2==0 numbers
 * from different sources and calculating total sum of such numbers
 * Every found valid number demonstrates to the user in real-time
 * Current total sum demonstrates to the user in real-time
 * @see .main
 * @author by Andrey Kostrov on 03.11.2016.
 */
public class BigHomeWork1 {

    /**
     * Container for mathematical sum.
     * All starting Thread put found valid values to this container
     * in their "synchronized" sections
     * At the program end totalSum demonstrates to the user
     */
    private static final BooleanBox proceedingFlag = new BooleanBox(true);
    private static final long refreshTime = 200L;
    private static final Logger logger = LoggerFactory.getLogger(BigHomeWork1.class);
    private static List<Parser> threads;

    /**
     * @param args array of Strings, each element of array is some resource name.
     *             can be URL or File
     */
    public static void main(String[] args) {
        logger.info("-------------Starting new session-------------");

        SourceDeterminator sourceDeterminator = new SourceDeterminator();

        // *** needs for whole-program-execution-time calculation
        long startTime = System.currentTimeMillis();

        // *** creating threads for every source
        threads = new LinkedList<>();
        try {
            for (String arg : args)
                threads.add(new Parser(sourceDeterminator.getBufferedReader(arg), arg, proceedingFlag));
            threads.forEach(Thread::start);
        }

         catch (IOException e) {
             proceedingFlag.setFALSE();
             threads.forEach(Thread::interrupt);

             logger.error("Stopping program. Error caused by -> {}", e.getMessage());

        } finally{
            while (checkThreadsState()){
                try{
                    Thread.sleep(refreshTime);
                    logger.trace("Total sum is {} .", getTotalSum() );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            String endProgramString = "Program ended in " + (System.currentTimeMillis() - startTime) + " milliseconds -> ";
                if (proceedingFlag.isTRUE()) {
                    logger.info(endProgramString + "successfully");
                }
                else
                    logger.warn(endProgramString + "unsuccessfully. See previous messages to find out a crash reason.");
        }
    }

    /**
     * Checking status of all working threads.
     * Interrupts all threads if proceeding flag is down
     * @return false if all threads are stopped or interrupted
     */
    private static boolean checkThreadsState(){
        synchronized (proceedingFlag) {
            if (proceedingFlag.isFALSE())
                threads.forEach(Thread::interrupt);
        }
        for (Thread currentThread: threads)
            if (currentThread.getState()!=Thread.State.TERMINATED)
                return true;
        return false;
    }

    /**
     * Total sum getter.
     * @return total sum, calculated in all threads
     */
    private static long getTotalSum(){
        long totalSum=0;
        for (Parser currentThread: threads){
            totalSum = totalSum + currentThread.getPrivateSum();
        }
        return totalSum;
    }
}