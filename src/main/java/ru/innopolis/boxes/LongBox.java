package ru.innopolis.boxes;

/**
 * Container for single long-primitive value *
 * @Author by Andrey Kostrov on 03.11.2016.
 */
public class LongBox {

    private long value;

    /**
     * standart-style Constructor
     * Creates instance containing zero-value
     */
    public LongBox() {this(0L);}

    /**
     * standart-style Constructor
     * Creates instance containing input value
     * @param value input value
     */
    public LongBox(long value) {
        this.value = value;
    }

    /**
     * standard-style getter
     * @return long-primitive value
     */
    public long getValue() {
        return value;
    }

    /**
     * standard-style setter
     * destroys previous container value and set input value
     * @param value absolute input value
     */
    public void setValue(long value) {
        this.value = value;
    }

    /**
     * increases current container value by input value
     * @param value input value for increasing container value
     */
    public void addValue(long value){
        this.value += value;
    }
}
