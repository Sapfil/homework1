package ru.innopolis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.boxes.BooleanBox;
import ru.innopolis.boxes.LongBox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Class is searching positive n%2==0 numbers, adding them to outer "total sum". *
 * @see SourceDeterminator - it creates BudderedReader for <tt>this class</tt>
 *                           from different source-types.
 * @author by Andrey Kostrov on 03.11.2016.
 */
public class Parser extends Thread{

    /**
     * BufferedReader created in SourceDeterminator
     * @see SourceDeterminator
     */
    private Reader inputReader;

    /**
     * Global "all is fine" flag
     * Can be dropped down by every thread
     */
    private final BooleanBox proceedingFlag;

    /** source name - needs real-time output and for Exception messages */
    private String source;
    /** containing current calculates sum for current source */
    private final LongBox privateSum;
    /** Standard logger  */
    private static Logger logger = LoggerFactory.getLogger(Parser.class);
    /** @see SynchronizedFoldingSummator */
    private SynchronizedFoldingSummator summator = new SynchronizedFoldingSummator(2);

    /**
     * Creates a new thread and then parses it
     * @param reader    BufferedReader created in SourceDeterminator
     * @param source    source name - needs real-time output and for Exception messages
     * @see SourceDeterminator
     */
    public Parser(Reader reader, String source, BooleanBox proceedingFlag){

        this.inputReader = reader;
        this.source = source;
        this.proceedingFlag = proceedingFlag;
        this.privateSum = new LongBox(0);
    }

    /**
     * Searching positive and (n%2 ==0) numbers in char-sequence from BufferedReader
     * External validator checks every symbol and returns type of symbol.
     * Symbol-types described in validators ENUM.
     * For number symbols - constructing values by rule (10*prevChar + current char).
     * When space found - calls synchronized(sum) summator for adding number value to total sum.
     * Total sum is private field of this class, there is synchronized(sum) getter for external usage.
     * @see SynchronizedFoldingSummator;
     * @see CharValidator;
     */
    @Override
    public void run(){

        // *** logic for parsing char-based input source
        int spaceChar = ' ';
        int currentSymbol = ' ', prevChar;

        boolean minusFound = false;
        int currentValue = 0;
        String finishMessage = null;

        try (BufferedReader reader = new BufferedReader(inputReader)){

            while (reader.ready()){

                prevChar = currentSymbol;
                currentSymbol = reader.read();

                switch (CharValidator.validateIntChar((char)currentSymbol)) {
                    case SPACE: {
                        if (!minusFound)
                            summator.addInputToTotal(currentValue, privateSum);
                        minusFound = false;
                        currentValue = 0;
                        break;
                    }
                    case NUMBER: {
                        //if (!minusFound) {
                            currentValue *= 10;
                            currentValue += currentSymbol - 48;
                        //}
                        break;
                    }
                    case MINUS: {
                        minusFound = true;
                        if (prevChar != spaceChar) {
                            synchronized (proceedingFlag) {
                                proceedingFlag.setFALSE();
                            }
                            throw new IOException("Wrong char-sequence. Minus-symbol is not after space-symbol in source " + source
                                    + "\nDropping down proceeding-flag.");
                        }
                        break;
                    }
                    case NOT_VALID:
                    default: {
                        synchronized (proceedingFlag) {
                            proceedingFlag.setFALSE();
                        }
                        throw new IOException("Not legal symbol " + "\" " + currentSymbol + " \"" +
                                "is source " + source + "\nDropping down proceeding-flag.");
                    }
                }
                // *** proceeding to next char
            }

            // *** end of source. If last char in sequence was number-char
            if (!minusFound && currentValue > 0) {
                this.summator.addInputToTotal(currentValue, privateSum);
            }
        } catch (IOException e) {
            synchronized (proceedingFlag){
                proceedingFlag.setFALSE();
            }
            logger.warn("Proceeding flag is down and parsing source {} is stopped. ", source);
            finishMessage = e.getMessage();
        } finally{
            if (finishMessage == null)
                logger.info("Source {} -> processed successfully.", source);
            else {
                logger.info("Source {} -> processed unsuccessfully with error: {}", source, finishMessage);
            }
        }
    }

    /**
     * Standard synchronized getter
     * @return current calculates sum
     */
    long getPrivateSum(){
        synchronized (privateSum){
            return privateSum.getValue();
        }
    }
}