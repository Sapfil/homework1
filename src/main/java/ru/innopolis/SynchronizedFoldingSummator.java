package ru.innopolis;

import ru.innopolis.boxes.LongBox;

/**
 * If input value%fold == 0 - adding value to output
 * Input value and outtput container are fields in
 * addInputToTotal-method;
 * @author Andrey Kostrov on 11.11.2016.
 */
public class SynchronizedFoldingSummator {

    /** If input value%fold == 0 - adding value to output */
    private int fold;

    /** crating summaror with fold = 1 */
    public SynchronizedFoldingSummator(){this(1);}
    /** creadting summator with custom fold */
    public SynchronizedFoldingSummator(int fold){
        this.fold = fold;
    };

    /** standard setted for fold-field */
    public void setFold(int fold){
        this.fold = fold;
    }

    /** standard setter for fold-field */
    public int getFold(){
        return fold;
    }

    /**
     * Adding inputValue to outer total sum
     * and writing some real-time info to console
     * @param inputValue value for adding to total sum
     */
    void addInputToTotal(long inputValue, LongBox totalValue){

        if (inputValue%fold == 0){
            synchronized (totalValue) {
                totalValue.addValue(inputValue);
            }
        }
    }
}
