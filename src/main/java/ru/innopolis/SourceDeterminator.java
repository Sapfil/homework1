package ru.innopolis;

import java.io.*;
import java.net.URL;

/**
 * Class determines type of input-source (file or URL)
 * and return BufferedReader based on input-source type.
 *
 * @author Andrey Kostrov on 04.11.2016
 */
public class SourceDeterminator {

    /**
     * Determines type of input-source (file or URL)
     * @param sourceName is String-name of source (File or URL)
     * @return BufferedReader based on input-source type.
     * @throws IOException if an I/O error occurs
     */
    public BufferedReader getBufferedReader(String sourceName) throws IOException {

        if (sourceName.startsWith("https://") || sourceName.startsWith("http://") ||
                sourceName.startsWith("ftp://") || sourceName.startsWith("file://localhost/"))
            return  createURLBufferedReader(sourceName);

        return createFileBufferedReader(sourceName);
    }

    /**
     * BufferedReader creation for URL-source
     * @param URLName input source
     * @return  BufferedReader
     * @throws IOException if an I/O error occurs
     */
    private BufferedReader createURLBufferedReader(String URLName) throws IOException {
          return new BufferedReader(new InputStreamReader(new URL(URLName).openStream()));
    }

    /**
     * BufferedReader creation for file-source
     * @param fileName input file path
     * @return BufferedReader
     * @throws FileNotFoundException if there is error in file path
     */
    private BufferedReader createFileBufferedReader(String fileName) throws FileNotFoundException {
        return
              new BufferedReader( new FileReader(fileName));
    }
}
