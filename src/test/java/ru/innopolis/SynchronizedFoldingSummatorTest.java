package ru.innopolis;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.boxes.LongBox;

import static org.junit.Assert.*;

/**
 * @author Andrey Kostrov on 12.11.2016.
 */
public class SynchronizedFoldingSummatorTest {

    private static SynchronizedFoldingSummator testSummator;

    /**
     * Initialization of test object
     */
    @BeforeClass
    public static void preparingTestSummator()   {
        testSummator = new SynchronizedFoldingSummator();
    }

    /**
     * Dropping test object to default
     */
    @Before
    public void setDefaultFold(){
        testSummator.setFold(1);
    }

    /**
     * Checking setter by using random input
     * @throws Exception if something goes wrong
     */
    @Test
    public void setFold() throws Exception {
        int testFold = (int)(Math.random()*Integer.MAX_VALUE);
        testSummator.setFold(testFold);
        assertEquals("Is fold set", testFold, testSummator.getFold());
    }

    /**
     * Checking getter in default object state
     * @throws Exception if something goes wrong
     */
    @Test
    public void getFold() throws Exception {
        assertEquals("Getting default fold", 1, testSummator.getFold());
    }

    /**
     * Setting random fold.
     * Trying to add 1 folding value and 2 unfolded.
     * Checking that only valid value processed.
     * @throws Exception if something goes wrong
     */
    @Test
    public void addInputToTotal() throws Exception {
        int testFold = (int)(Math.random()*10) + 2;
        testSummator.setFold(testFold);
        int foldingValue = (int)(Math.random()*10)*testFold;
        int unfoldingValue1 = foldingValue - 1;
        int unfoldingValue2 = foldingValue + 1;
        LongBox testSum = new LongBox(0);
        testSummator.addInputToTotal(foldingValue, testSum);
        testSummator.addInputToTotal(unfoldingValue1, testSum);
        testSummator.addInputToTotal(unfoldingValue2, testSum);
        assertEquals("Is valid value added to sum", foldingValue, testSum.getValue());
    }
}