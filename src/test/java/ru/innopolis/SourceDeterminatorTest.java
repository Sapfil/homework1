package ru.innopolis;

import org.junit.*;

import java.io.*;

import static org.junit.Assert.*;

/**
 * "All-in-one"-style test.
 * Creates test file, containing some test string
 * Trying to create BufferedReader wrapping this file
 * 1'st time - like a file
 * 2'nd time - like a URL (emulated by adding "file://lockalhost/"-prefix)
 * Trying to read test string from a file.
 *
 * @author  Andrey Kostrov on 06.11.2016/ last edit 13.11.16
 */
public class SourceDeterminatorTest {

    private static String systemAbsPathForTestFile;
    private static BufferedReader testReader;
    private static String testString;

    @BeforeClass
    public static void openingTestFile(){
        File testFile = new File("src/test/java/ru/innopolis/test.txt");
        systemAbsPathForTestFile = testFile.getAbsolutePath();
        try {
            testReader= new BufferedReader(new FileReader(systemAbsPathForTestFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (BufferedReader reader = new BufferedReader( new FileReader(testFile))){
            testString = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getBufferedReader() throws Exception {

        SourceDeterminator testSD = new SourceDeterminator();

        assertEquals("Testing 'file' source type",
                testSD.getBufferedReader(systemAbsPathForTestFile).readLine(),
                testString );

        assertEquals("Testing 'URL' source type",
                testSD.getBufferedReader("file://localhost/" + systemAbsPathForTestFile).readLine(),
                testString );
    }

    @AfterClass
    public static void closingTestFile(){
        try {
            testReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}