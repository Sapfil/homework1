package ru.innopolis;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Andrey Kostrov on 12.11.2016.
 */
public class CharValidatorTest {

    /**
     * Ckeking all Unicode-char-table validation
     * @throws Exception if something goes wrong
     */
    @Test
    public void testValidateIntChar() throws Exception {
        for (char ch = 0; ch < 65535; ch++) {
            if (ch > 47 && ch < 58)
                assertEquals("Number validation check", CharValidator.symbolType.NUMBER, CharValidator.validateIntChar(ch));
            else if (ch == 32)
                assertEquals("Space validation check", CharValidator.symbolType.SPACE, CharValidator.validateIntChar(ch));
            else if (ch == 45)
                assertEquals("Minus validation check", CharValidator.symbolType.MINUS, CharValidator.validateIntChar(ch));
            else
                assertEquals("NotValid symbol check", CharValidator.symbolType.NOT_VALID, CharValidator.validateIntChar(ch));
        }
    }
}